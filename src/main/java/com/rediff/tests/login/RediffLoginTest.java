package com.rediff.tests.login;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.rediff.utils.Common;
import com.rediff.utils.ExcelReader;

public class RediffLoginTest extends Common{

	@BeforeMethod
	public void openBrowser() {
		instanciateBrowsers();
	}

	@Test(priority = 1)
	public void verifyPageTitle() throws IOException {
		try {
		SoftAssert soft=new SoftAssert();
		String actualPageTitle = driver.getTitle();
		String expectedPageTile = "Rediff.com: News | Rediffmail | Stock Quotes | Shopping";
		soft.assertEquals(actualPageTitle, expectedPageTile);
		System.out.println("Page Title verification complelted");
		soft.assertAll();
		}catch(Exception e) {
			captureScreenshot();
			e.printStackTrace();
		}
	}

	@Test(dependsOnMethods = "verifyPageTitle",priority = 2,dataProvider = "data",enabled=true)
	public void verifyInvalidLogin(String username,String password) throws IOException {
		try {
		// 3. Click on 'Sign in' link
		driver.findElement(By.linkText("Sign in")).click();
		// 4. enter invalid user name 'sdfdsfds@sdfds.co'
		driver.findElement(By.id("login1")).sendKeys(username);
		// 5. enter invalid password 'sdfdsfd'
		driver.findElement(By.id("password")).sendKeys(password);
		// 6. click on sign in button
		driver.findElement(By.name("proceed")).click();
		// 7. extract the error message and validate it
		String actualErrorMessage = driver.findElement(By.id("div_login_error")).getText();
		String expectedErrorMessagge = "Wrong username and password combination.";
        Assert.assertEquals(actualErrorMessage, expectedErrorMessagge);
		System.out.println("Verifying Invalid login with username: "+username+" and password: "+password);
		}catch(Exception e) {
			captureScreenshot();
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void closeBrowser() {
		closebrowsers();
	}
	
	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException{
		ExcelReader excel=new ExcelReader();
		return excel.readExcelData();
	}

}
