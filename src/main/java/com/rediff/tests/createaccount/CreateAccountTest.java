package com.rediff.tests.createaccount;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rediff.utils.Common;

public class CreateAccountTest extends Common{
//CreateAccount Test class to test the module...
	@BeforeMethod
	public void openBrowser() {
		instanciateBrowsers();
	}
    @Test
	public void createAccount() throws InterruptedException, IOException {
    	try {
		driver.findElement(By.linkText("Create Account")).click();
		driver.findElement(By.xpath("//td[contains(text(),'Full Name')]/parent::tr/td[3]/input")).sendKeys("rajesh");
		
		driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[1]")).sendKeys("rajesh");
		
		driver.findElement(By.xpath("//td[contains(text(),'Choose a Rediffmail ID')]/parent::tr/td[3]/input[2]")).click();
		Thread.sleep(1500);
		String actualErrorMessage=driver.findElement(By.id("check_availability")).getText();
		String expectetdErrorMessage="Sorry, the ID that you are looking for is taken.";
		
		System.out.println("actualErrorMessage: "+actualErrorMessage);
		System.out.println("expectetdErrorMessage: "+expectetdErrorMessage);
       // Assert.assertTrue(actualErrorMessage.contains(expectetdErrorMessage), "Error message not matched");
		Assert.assertFalse(actualErrorMessage.contains(expectetdErrorMessage),"Error messagges are matching");
		
		//True ---> True ===> PASS
		//False ---> False ===> PASS
		System.out.println("Verifying Email availibility is completed");
    	}catch(Exception e) {
    		captureScreenshot();
    		e.printStackTrace();
    	}
	}

	@AfterMethod
	public void closeBrowser() {
	closebrowsers();
	}
}
