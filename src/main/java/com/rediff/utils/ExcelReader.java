package com.rediff.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class ExcelReader extends Common{

	public Object[][] readExcelData() throws EncryptedDocumentException, IOException{
		InputStream file=new FileInputStream(path+"\\src\\main\\resources\\TestData.xlsx");
		Workbook workbook =WorkbookFactory.create(file);
		Sheet sheet=workbook.getSheetAt(0);
		
		int lastRowNo=sheet.getLastRowNum();
		int totalRowsCount=lastRowNo+1;
		
		Row row =sheet.getRow(0);
		int totalCellCount=row.getLastCellNum();
		
		Object o[][]=new Object[totalRowsCount][totalCellCount];
		//Rows
		for(int i=0;i<totalRowsCount;i++) {
			row =sheet.getRow(i);
			totalCellCount=row.getLastCellNum();
			//Cells in a Row
			for(int j=0;j<totalCellCount;j++) {
				Cell cell=row.getCell(j);
				String cellValue=cell.getStringCellValue();
				//System.out.println(cellValue);
				o[i][j]=cellValue;
			}
		}
		return o;
	}
	
	
	
	
	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		ExcelReader reader=new ExcelReader();
		reader.readExcelData();
	}

}
