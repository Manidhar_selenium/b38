package com.rediff.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;


public class Common {
	public WebDriver driver = null;
	public Properties environments=null;
	public Logger log=null;
	
	public String path=System.getProperty("user.dir");

	@BeforeSuite
	public void loadPropFiles() throws IOException {
		log=Logger.getLogger("rootLogger");
		log.debug("Logger configured successfully");
		System.out.println(path);
		InputStream envFile = new FileInputStream(path+"\\src\\main\\resources\\EnvironmentDetails.properties");
		environments = new Properties();
		environments.load(envFile);
		System.out.println("Environment details property file loaded");
	}
	
	@AfterSuite
	public void clearPropFiles(){
		environments.clear();
		System.out.println("Clearing the Environment property file");
	}

	public void instanciateBrowsers() {

		String browserName =environments.getProperty("browser");

		if (browserName.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver",path+"\\src\\main\\resources\\drivers\\windows\\chrome\\chromedriver.exe");
			driver = new ChromeDriver();
			System.out.println("Opened Chrome Browser");
		} else if (browserName.equalsIgnoreCase("Firefox")) {
			System.setProperty("webdriver.gecko.driver",path+"\\src\\main\\resources\\drivers\\windows\\firefox\\geckodriver.exe");
			driver = new FirefoxDriver();
			System.out.println("Opened Firefox Browser");

		} else if (browserName.equalsIgnoreCase("Edge")) {
			System.setProperty("webdriver.edge.driver",path+"\\src\\main\\resources\\drivers\\windows\\edge\\msedgedriver.exe");
			driver = new EdgeDriver();
			System.out.println("Opened Edge Browser");

		} else if (browserName.equalsIgnoreCase("Opera")) {
			// Opera logic here....
		} else if (browserName.equalsIgnoreCase("Safari")) {
			// Safari logic here...
		} else {
			throw new Error("please cross check the browser name");
		}

		// 2. navigate to https://www.rediff.com/
		driver.get(environments.getProperty("url"));
		System.out.println("Navigating to application");
	}

	public void closebrowsers() {
		// 8. close the browser
		driver.close();
		System.out.println("Closing Browser");

	}

	public void captureScreenshot() throws IOException {
		TakesScreenshot screen = ((TakesScreenshot) driver);
		File sourceContent = screen.getScreenshotAs(OutputType.FILE);

		FileUtils.copyFile(sourceContent,
				new File(path+"\\src\\main\\resources\\Screenshots\\Screen1.png"));
	}
}
